package org.example.config;

import org.example.Entity.Client;
import org.example.Entity.Product;
import org.example.Entity.Supplier;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DatabaseConfig {
    private static SessionFactory sessionFactory = null;


    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.configuration.xml")
                    .addAnnotatedClass(Client.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Supplier.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
