package org.example.Entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    private Integer id;
    @Column(name = "product_type")
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;
    private String name;
    private String description;
    private Integer quantity;
    @Column(name = "buying_price")
    private Double buyingPrice;
    @Column(name = "selling_price")
    private Double sellingPrice;
    @Column(name = "buying_date")
    private LocalDate buyingDate;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;
}

