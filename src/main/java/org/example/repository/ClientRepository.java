package org.example.repository;


import org.example.Entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {
    private final SessionFactory sessionFactory;
    public ClientRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    public void save(Client client){
        Session session = sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        session.persist(client);
        transaction.commit();
        session.close();
    }
  public List<Client> getAll(){
        List<Client>clients = new ArrayList<>();
        Session session = sessionFactory.openSession();
        //sql: SELECT * FROM client;
        //acesta este un query de Hibernate
      //hql: SELECT c FROM Client c;
      clients = session.createQuery("SELECT c FROM Client c",Client.class).getResultList();
      session.close();
      return clients;
  }
}

