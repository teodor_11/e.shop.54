package org.example.repository;

import org.example.Entity.Product;
import org.example.Entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ProductRepository {
    private final SessionFactory sessionFactory;
    public ProductRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    public void save(Product product){
        Session session = sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        session.persist(product);
        transaction.commit();
        session.close();
    }
    public List<Product> getAll (){
        List<Product>products;
        Session session = sessionFactory.openSession();
        products = session.createQuery("SELECT p FROM product p", Product.class).getResultList();
        session.close();
        return products;
    }
}

